## Linux commands.
sudo systemctl stop postgresql.service    : Stop DB Server.
sudo systemctl start postgresql.service   : Start DB Server.
sudo systemctl enable postgresql.service  : Enable DB Server.
sudo systemctl status postgresql.service  : Show DB Server Status.
sudo passwd postgres
sudo su -l postgres
psql                                      : Starts PSQL

## PSQL Commands
\list              : List all databases.
\dt                : Show all relations.
\password password : Change admin password.